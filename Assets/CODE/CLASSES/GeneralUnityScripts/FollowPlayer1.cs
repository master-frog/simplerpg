﻿using UnityEngine;


namespace UnityCommunityProject
{
    public class FollowPlayer1 : MonoBehaviour
    {
        // hard-wire this reference in the Inspector
        public Transform _player;

        private Vector3 _offset;

        void Start()
        {
            _offset = transform.position - _player.position;
        }

        void LateUpdate()
        {
            this.transform.position = _player.position + _offset;
        }
    }
}