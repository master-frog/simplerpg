﻿using UnityEngine;
using System.Collections;

namespace DanielQuick {

	/// <summary>
	/// Used to display a message onto the UI, utilizing SignCanvas
	/// Activates when player enters the InteractTrigger
	/// </summary>
	public class Sign : MonoBehaviour, IInteract {

		[SerializeField][Multiline]
		private string message;

		[SerializeField]
		private bool showInBuilds = true;

		void Awake() {
			if(!Application.isEditor && !showInBuilds) {
				Destroy(gameObject);
			}
		}

		public void Interact() {
			SignCanvas.Instance.ShowMessage(message);
		}

		public void EndInteract() {
			SignCanvas.Instance.HideMessage(message);
		}

	}

}